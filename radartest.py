import math

#https://www.radartutorial.eu/01.basics/The%20Radar%20Range%20Equation.en.html
def w2dbm(W):
    return 10.*math.log10(W*1000.0)

def dbm2w(dBm):
    return (10**((dBm)/10.))*1000.0

def mhz2wl(mhz):
    return 299792458.0 / (mhz*1000000)

def wl2mhz(wl):
    return (299792458.0 / wl) / 1000000

def gmu2m(gmu):
    return gmu * 0.01905

def m2gmu(m):
    return m / 0.01905

antennas = {"ra_domestic_dish": {'directional':True,
                    'type':'parabolic',
                    'gain':25.0,
                     'bw':5.0,
                     'pol':2,
                     'area':0.8413},
            "ra_large_drum": {'directional':True,
                    'type':'parabolic',
                    'gain':22.0,
                     'bw':5.0,
                     'pol':0,
                     'area':4.21},
            "ra_log": {'directional':True,
                    'type':'yagi',
                     'gain':9.6,
                     'bw':20.0,
                     'pol':2,
                     'area':0.093},
            "ra_sector": {'directional':True,
                     'type':'parabolic',
                     'gain':16,
                     'bw':120,
                     'pol':0,
                     'area':0.342},
            "ra_orbital_dish": {'directional':True,
                    'type':'parabolic',
                     'gain':20,
                     'bw':15,
                     'pol':1,
                     'area':1.235},
            "ra_panel": {'directional':True,
                    'type':'panel',
                     'gain':19.5,
                     'bw':8,
                     'pol':1,
                     'area':0.465},
            "ra_small_drum": {'directional':True,
                    'type':'parabolic',
                     'gain':17.5,
                     'bw':5,
                     'pol':0,
                     'area':0.171},
            "ra_uplink_dish": {'directional':True,
                    'type':'parabolic',
                     'gain':32,
                     'bw':5,
                     'pol':2,
                     'area':4.208},
            "radar_small": {'directional':True,
                     'type':'array',
                    'type':'parabolic',
                     'gain':17.5,
                     'bw':5,
                     'pol':0,
                     'area':0.235,
                     'size':21},
            "radar_mid": {'directional':True,
                     'type':'array',
                     'gain':17.5,
                     'bw':5,
                     'pol':0,
                     'area':0.391,
                     'size':21},
            "radar_big": {'directional':True,
                     'type':'array',
                     'gain':17.5,
                     'bw':5,
                     'pol':0,
                     'area':0.688,
                     'size':28},
            "radar_sp_small": {'directional':True,
                     'type':'array',
                     'gain':17.5,
                     'bw':5,
                     'pol':0,
                     'area':1.834,
                     'size':36.6},
            "radar_sp_mid": {'directional':True,
                     'type':'array',
                     'gain':17.5,
                     'bw':5,
                     'pol':0,
                     'area':2.611,
                     'size':58},
            "radar_sp_big": {'directional':True,
                     'type':'array',
                     'gain':17.5,
                     'bw':5,
                     'pol':0,
                     'area':6.957,
                     'size':95}
            }

model = 'radar_sp_small'
ant = antennas[model]
watt = 4000.0
frequency = 1000 #mhz, 1-30000 (1MHz-30GHz)
rcs = 1 #m^2
area = ant['area'] # m^2
size =  gmu2m(ant['size']) # m
efficiency = 0.5 #0.7
pulsetime_us = 0.1 #fan song: 0.4-1.2
dist_mod = 10 #2.5
directionality = 1 # = default, 10 = directional
bearing_accuracy = 1 #0.05 - 0.25
attenuation = 0.00001333 # 0.00001333 * MHz * KM

dist = 10
print('antenna:',model)
print('power:',watt,'watts')
print('frequency:',frequency,'mhz')
print('aperture size:',area,'m^2')
print('antenna size:',size,'m')
wavelength = mhz2wl(frequency)
print('wavelength:',round(wavelength,4),'m',round(wavelength*29.527575))
rangeres = ((pulsetime_us/1000000.0) * 299792458) / 2 / dist_mod

wl2rcs = rcs / wavelength
print('wavelength to RCS ratio',round(wl2rcs,4))
    
print('range resolution:',round(rangeres,4),'m',round(rangeres*29.527575),'gmu')

gain =  ((4 * math.pi * efficiency * area) / math.pow(wavelength,2)) * (math.pow(directionality,2))
print('gain:',round(10*math.log10(gain),2),'dbm')
beamwidth = ((51 * wavelength) / size) / directionality
if beamwidth>360: beamwidth = 360
print('parabolic beamwidth:', round(beamwidth,2),'deg')
input()
dbm = 1



while dbm> -90 and m2gmu(dist) < 60000:
    moddist = dist * dist_mod
    print()
    print('range:',dist,'m',round(m2gmu(dist)),'gmu')
    ndpd = watt / (4 * math.pi * math.pow(moddist,2))
    #print('ndpd', ndpd)

    dpd = ndpd * gain
    #print('dpd',dpd)

    rp = dpd * rcs
    #print('rpW',rp)

    rpd = rp / (4 * math.pi * math.pow(moddist ,2))
    #print('rpd',rpd)

    rp2 = rpd * area * efficiency
    #print('rp2',rp2)

    # rcs calc https://www.radartutorial.eu/01.basics/Radar%20Cross%20Section.en.html
    refp = rp / (4 * math.pi * math.pow(moddist,2))
    print('rcs',round(w2dbm(refp),2),'dbm')

    bwatdist = math.sqrt( math.pow(moddist,2) + math.pow(moddist,2) - 2.0 * math.pow(moddist,2) * math.cos(math.radians(beamwidth)) ) / dist_mod
    print('beamwidth at dist',round(bwatdist,4),'m', round(m2gmu(bwatdist)),'gmu')
    #wl2bwat = bwatdist/wavelength
    #rcs2bw = rcs / bwatdist
    #print('wl2bwat',round(wl2bwat))
    #print('rcs2bw',round(rcs2bw,4))
    #poserror = round(wl2bwat) / rcs

    poserror = (math.sqrt( math.pow(moddist,2) + math.pow(moddist,2) - 2.0 * math.pow(moddist,2) * math.cos(math.radians(bearing_accuracy/2)) ) * 2 ) / wl2rcs
    print('position error:',round(poserror),'gmu')
    
    # not simulated
    #angres = wavelength*dist / (math.sqrt(area))
    #print('angres:',round(angres,4),'m', round(angres*29.527575),'gmu')

    dbm = w2dbm(rp2)
    print('dBm',round(dbm,2))
    dist+=10

