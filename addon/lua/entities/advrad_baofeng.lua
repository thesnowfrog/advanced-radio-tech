AddCSLuaFile()

DEFINE_BASECLASS( "advrad_base_dumb" )

ENT.PrintName = "Baofeng UV-5R"
ENT.Author = "snowfrog"
ENT.Spawnable = true
ENT.AdminSpawnable = true 
ENT.Information	 = "5W handheld radio" 
ENT.Category = "Advanced Radio"
ENT.Model = "models/radio/w_radio.mdl"
ENT.Mass = 5

function ENT:Initialize()
 if (SERVER) then
     self:LoadModel()
     self:PhysicsInit( SOLID_VPHYSICS )
     self:SetSolid( SOLID_VPHYSICS )
     self:SetMoveType( MOVETYPE_VPHYSICS )
     self:SetUseType( ONOFF_USE ) -- doesen't fucking work
     self.EntList={}
     self.EntCount = 0
     local phys = self:GetPhysicsObject()
     local skincount = self:SkinCount()
     if (phys:IsValid()) then
         phys:SetMass(self.Mass)
         phys:Wake()
     end
 end
end

function ENT:SpawnFunction( ply, tr )
     if ( not tr.Hit ) then return end
     local ent = ents.Create( self.ClassName )
     ent:SetPhysicsAttacker(ply)
     ent:SetPos( tr.HitPos + tr.HitNormal * 26 ) 
     ent:Spawn()
     ent:Activate()
     return ent
end 

function ENT:Use(activator,caller)
	if (activator.advrad.baofeng) then
        activator.advrad.baofeng = true
		self.Entity:Remove()
	end
end

