AddCSLuaFile()
DEFINE_BASECLASS( "base_anim" )

--[[
This is a base for vegetable entities which are not affected by bullets and shit.
OMFG STOP READING THIS!
--]]


local Models = {}
Models[1]                            =  "model"

ENT.Spawnable		            	 =  false         
ENT.AdminSpawnable		             =  false         

ENT.PrintName		                 =  "Name"       
ENT.Author			                 =  "Avatar natsu"     
ENT.Contact			                 =  "GTFO" 
ENT.Category                         =  "GTFO!"           

ENT.Model                            =  ""            
ENT.Life                             =  20       
ENT.Mass                             =  0

function ENT:Initialize()
    if (SERVER) then
        self:LoadModel()
        self:PhysicsInit( SOLID_VPHYSICS )
        self:SetSolid( SOLID_VPHYSICS )
        self:SetMoveType( MOVETYPE_VPHYSICS )
        self:SetUseType( ONOFF_USE ) -- doesen't fucking work
        local phys = self:GetPhysicsObject()
        local skincount = self:SkinCount()
        if (phys:IsValid()) then
            phys:SetMass(self.Mass)
            phys:SetBuoyancyRatio(0)
            phys:Wake()
        end
    end
end

function ENT:LoadModel()
     self:SetModel(self.Model)
end

function ENT:PhysicsCollide( data, physobj )
end

function ENT:OnRemove()
	 self:StopParticles()
end

if ( CLIENT ) then
     function ENT:Draw()
         self:DrawModel()
     end
end
