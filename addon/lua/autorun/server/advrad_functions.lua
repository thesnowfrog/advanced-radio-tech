AddCSLuaFile()


-- hooks
hook.Add("PlayerDeath","advrad_death", advrad_clearPlyTable)

hook.Add( "PlayerSay", "RadioChat", function( ply, text, team )
    PrintMessage( HUD_PRINTCONSOLE, text)

    if ply.advrad.baofeng then
    
        if ( text.StartWith(string.lower(text), "/radiofreq")) then
            local message = string.gsub(text,"/radiofreq","")
            ply:PrintMessage( HUD_PRINTTALK, "Radio frequency set to "..tonumber(message).." MHz")
            ply.advrad.freq = tonumber(message)

        elseif ( text.StartWith(string.lower(text), "/radiopower")) then
            local message = string.gsub(text,"/radiopower","")
            ply:PrintMessage( HUD_PRINTTALK, "Radio power set to "..tonumber(message).." watts")
            ply.advrad.txpower = tonumber(message)
            
        elseif ( text.StartWith(string.lower(text), "/radiocrypto")) then

        elseif ( text.StartWith(string.lower(text), "/radiokey")) then 
        
        elseif ( text.StartWith(string.lower(text), "/radio")) then
            local message = text.TrimRight(text,"/radio")
            ply:PrintMessage( HUD_PRINTTALK, "Radio message "..message)
        end
        
        
    end
end )

-- this function initializes the table on each player
function advrad_makePlyTable(ply)
    if not ply.advrad then
        ply.advrad = {
            baofeng = 0,
            freqmhz = 0,
            txwatts = 0,
            crypto = 0,
            key = 0,
            gain = 2.1,
            pol = 1,
            beamWidth = 360.0
        }
    end
end


function advrad_clearPlyTable(ply)
    advrad_makePlyTable(ply)
    ply.advrad.baofeng = 0
    ply.advrad.freqmhz = 0
    ply.advrad.txwatts = 0
    ply.advrad.crypto = 0
    ply.advrad.key = 0
    ply.advrad.gain = 2.1
    ply.advrad.pol = 1
    ply.advrad.beamWidth = 360.0
end

-- Allow an antenna to query for tx power
function TxDbw(ENT)
	return ENT.advrad.gain + (math.log10(ENT.advrad.txwatts)*10)
end


-- Can we transmit? (Got enough resources?)
function CanTX(ENT)
	return true
end

-- Returns the background noise at this location in decibels relative to one milliwatt
function GetBgNoise(ENT)
	local firenoise = 0
	for k, v in pairs(ents.FindInSphere(ENT:GetPos(), 1000)) do
		if v:IsOnFire() then
			firenoise = firenoise + 1000/ENT:GetPos():Distance(v:GetPos())
		end
	end
	return math.random() + firenoise
end

-- this is called whenever a wire input changes value
function TriggerInput(ENT,iname, value)
	if self.is_tx then
		if iname == "On" then
			self.active = (value ~= 0)
		elseif iname == "TxWatts" then
			local m = GetConVarNumber("sv_wdrk_max_tx_power")
			if value > m then
				self.txwatts = m
			elseif value <= 0 then
				self.txwatts = 0
			else
				self.txwatts = value
			end
		elseif iname == "BaseMHz" then
			-- someone has changed the base frequency, update the frequencies
			-- of all the channels to be based on the new value
			self.txchannels = {}
			self.txchannels[self.Inputs.BaseMHz.Value + 2.5] = self.Inputs.Channel1.Value
			self.txchannels[self.Inputs.BaseMHz.Value + 7.5] = self.Inputs.Channel2.Value
			self.txchannels[self.Inputs.BaseMHz.Value + 12.5] = self.Inputs.Channel3.Value
			self.txchannels[self.Inputs.BaseMHz.Value + 17.5] = self.Inputs.Channel4.Value
		elseif iname == "Channel1" then
			self.txchannels[self.Inputs.BaseMHz.Value + 2.5] = value
		elseif iname == "Channel2" then
			self.txchannels[self.Inputs.BaseMHz.Value + 7.5] = value
		elseif iname == "Channel3" then
			self.txchannels[self.Inputs.BaseMHz.Value + 12.5] = value
		elseif iname == "Channel4" then
			self.txchannels[self.Inputs.BaseMHz.Value + 17.5] = value
		end
	end
end


function Randomize(ENT)
	for i=1, 4 do
		local c = tostring(i)
		Wire_TriggerOutput(self.Entity, "Channel" .. c, math.random() * 10)
		Wire_TriggerOutput(self.Entity, "Ch" .. tostring(i) .. "_HasSignal", 0)
		Wire_TriggerOutput(self.Entity, "Ch" .. c .. "_dBm", -math.random()*1000)
	end
end




