AddCSLuaFile()
WIRE_DIRECTIONAL_RADIO_KIT = true -- Use this in your own Lua code to check for the presence of the Wire Directional Radio Kit

CreateConVar("advrad_scale", 180.0) -- Map Scale Adjustment factor
CreateConVar("advrad_max_tx_power", 10000.0) -- Maximum transmit power in Watts
CreateConVar("advrad_require_caf", 0) -- Require Custom Addon Framework resources if available
CreateConVar("advrad_rx_sensitivity_threshold", -90.0) -- Receive sensitivity in dbm
