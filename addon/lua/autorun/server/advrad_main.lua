AddCSLuaFile()

-- main timer
timer.Create( "advrad_think", 0.05, 0, function() -- 0.05 second timer, infinite repetitions

    for _, ply in pairs( player.GetAll() ) do
        if not ply.advrad then
            advrad_makePlyTable(ply)
        else

            -- Find all antennas on the map
            local ants = ents.FindByClass("advrad_*")

            -- Lists of discovered transmitters
            local txs = {}

            -- Loss due to angle offset
            local angleloss = 0

            -- Loss due to polarity skew
            local skewloss = 0

            -- If we found antennas
            if ants and #ants > 0 then
                for k, v in pairs(ants) do

                    -- Find the vector from the receiver to the transmitter and vice-versa
                    local vecToTx = v:GetPos() - self:GetPos()
                    local vecFromTx = self:GetPos() - v:GetPos()

                    -- Normalize the above to obtain the direction both ways
                    local normVectToTx = vecToTx:GetNormalized()
                    local normVectFromTx = vecFromTx:GetNormalized()

                    -- Find the direction of the receiver and transmitter
                    local myAngle = self:GetForward()
                    local txAngle = v:GetForward()

                    -- Find the loss due to polarity skew
                    -- If both antennas are not cross polarity, determine the loss due to skew
                    if self.pol ~= 0 and v.pol ~= 0 then
                        -- Both are the same polarity by default
                        local skew = 0
                        -- One vertical, one horizontal
                        if self.pol ~= v.pol then skew = 1.5707963267949 end
                        
                        skewloss = math.abs(math.sin(math.rad(v:GetAngles().r) - math.rad(self:GetAngles().r) + skew) * 20)
                    end

                    local onedir = math.abs(math.acos(normVectToTx:DotProduct(myAngle)))
                    local otherdir = math.abs(math.acos(txAngle:DotProduct(normVectFromTx)))

                    angleloss = (onedir + otherdir) * 30

                    -- Calculate the received signal strength (strength + self.gain)
                    -- If this transmitter is operational, within our field of vision, and we are within its beam
                    if v:CanTX() and math.deg(onedir) <= (self.beamWidth/2.0) and math.deg(otherdir) <= (v.beamWidth/2.0) then
                        table.insert(txs, v)
                    end
                end

                local spectrum = {}

                if #txs == 0 then
                    self:Randomize()
                    return
                end

                for k, v in pairs(txs) do
                    local dist = self:GetPos():Distance(v:GetPos()) / GetConVarNumber("advrad_scale")
                    local dBm = (math.log10((10^(v:TxDbw()/10)) / (4 * math.pi * dist * dist)) * 10) + 30
                    for freq, signal in pairs(v.txchannels) do
                        if spectrum[freq] == nil then
                            spectrum[freq] = {}
                            spectrum[freq][dBm] = signal
                        else
                            for sfreq, stable in pairs(spectrum) do
                                if freq == sfreq then
                                    stable[dBm] = signal
                                end
                            end
                        end
                    end
                end

                -- The set of channels that have detected a signal
                local setset = {false, false, false, false}

                for freq, t in pairs(spectrum) do
                    -- Are any of my receiving frequencies in this table?
                    if freq >= self.Inputs.BaseMHz.Value and freq < self.Inputs.BaseMHz.Value + 40 then

                        -- Allow for signal loss due to misaligned tuner frequency
                        local driftloss = 0 -- dB
                        local choffset = freq - self.Inputs.BaseMHz.Value
                        if choffset >= 0 and choffset < 5 then
                            driftloss = 10 * math.abs(2.5 - choffset)
                        elseif choffset >= 5 and choffset < 10 then
                            driftloss = 10 * math.abs(7.5 - choffset)
                        elseif choffset >= 10 and choffset < 15 then
                            driftloss = 10 * math.abs(12.5 - choffset)
                        elseif choffset >= 15 and choffset < 20 then
                            driftloss = 10 * math.abs(17.5 - choffset)
                        end

                        -- Initialize the signal and the strength
                        local sig, dBm = 0, -100
                        -- Count the number of received signals on this frequency
                        local count = 0; for k,v in pairs(spectrum[freq]) do count = count + 1 end

                        if count == 1 then
                            for k, v in pairs(spectrum[freq]) do dBm = k; sig = v; end
                        elseif count > 1 then
                            -- Initialize the strongest and second strongest received signals on this frequency
                            local top = -999 -- dBm
                            local lower = -1000 -- dBm

                            -- Find the highest and second highest strength signals on this frequency
                            for k, v in pairs(spectrum[freq]) do if k > top then top = k end end
                            for k, v in pairs(spectrum[freq]) do if k > lower and k < top then lower = k end end

                            -- Find the strength of the received signal as being the strongest - the second strongest
                            dBm = top - lower

                            -- We have a signal lock, return the strongest signal being carried by this frequency
                            sig = spectrum[freq][top]
                        end

                        dBm = dBm + self.gain - driftloss - angleloss - skewloss - self:GetBgNoise()

                        local signalLock = 1

                        -- If the received signal after noise is less than the receiver's sensitivity threshold, then the data received is just random noise
                        if dBm < GetConVarNumber("advrad_rx_sensitivity_threshold") then
                            sig = math.random() * 1000
                            signalLock = 0
                        end

                        local receiveCh = 0

                        if freq >= self.Inputs.BaseMHz.Value and freq < self.Inputs.BaseMHz.Value + 5 then
                            receiveCh = 1
                        elseif freq >= self.Inputs.BaseMHz.Value + 5 and freq < self.Inputs.BaseMHz.Value + 10 then
                            receiveCh = 2
                        elseif freq >= self.Inputs.BaseMHz.Value + 10 and freq < self.Inputs.BaseMHz.Value + 15 then
                            receiveCh = 3
                        elseif freq >= self.Inputs.BaseMHz.Value + 15 and freq < self.Inputs.BaseMHz.Value + 20 then
                            receiveCh = 4
                        end

                        if receiveCh ~= 0 then
                            local c = tostring(receiveCh)
                            Wire_TriggerOutput(self.Entity, "Channel" .. c, sig)
                            Wire_TriggerOutput(self.Entity, "Ch" .. c .. "_HasSignal", signalLock)
                            Wire_TriggerOutput(self.Entity, "Ch" .. c .. "_dBm", dBm)
                            setset[receiveCh] = true
                        end
                    end
                end

                -- set the remaining channels randomly
                for i=1,4 do
                    if not setset[i] then
                        Wire_TriggerOutput(self.Entity, "Channel" .. tostring(i), math.random() * 10)
                        Wire_TriggerOutput(self.Entity, "Ch" .. tostring(i) .. "_HasSignal", 0)
                        Wire_TriggerOutput(self.Entity, "Ch" .. tostring(i) .. "_dBm", -math.random()*1000)
                    end
                end
            end
    
        end
    end
end)
